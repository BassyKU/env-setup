#!/bin/bash
# Easy installation on Ubuntu 14.04 server

### Requirements ###
sudo apt-get -y install python-numpy
if [ $? -eq 100 ]; then 
	sudo mv /var/cache/apt/archives/lock /var/cache/apt/archives/lock_bak
	sudo apt-get -y install python-numpy
fi
sudo apt-get -y install cython python-scipy python-dev python-pip python-nose g++ libopenblas-dev git libblas-dev liblapack-dev libatlas-base-dev gfortran 

### Install Theano, Lasagne, Nolearn ###
# If you want to install local packages, please use `virtualenv` or use `pip install --user`
sudo pip install -r https://raw.githubusercontent.com/dnouri/nolearn/master/requirements.txt
sudo pip install nolearn==0.5b1 #Don't install the current version!
