#!/bin/bash

# general dependencies
apt-get --yes --force-yes install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
apt-get --yes --force-yes install --no-install-recommends libboost-all-dev

# CUDA: skipped

# install openBLAS
apt-get install git python-dev gfortran
mkdir ~/src
cd ~/src
git clone https://github.com/xianyi/OpenBLAS
cd OpenBLAS
make FC=gfortran
make PREFIX=/opt/openblas install
sh -c "echo '/opt/openblas/lib/' > /etc/ld.so.conf.d/openblas.conf"
ldconfig


# install Python headers
apt-get --yes --force-yes install python-dev


# remaining dependencies
apt-get --yes --force-yes install libgflags-dev libgoogle-glog-dev liblmdb-dev


# install caffe
cd ~/src
git clone https://github.com/BVLC/caffe
cd caffe
cp Makefile.config.example Makefile.config
sed -i "s/\(BLAS *:= *\).*/\1open/" Makefile.config
sed -i "s/\(# BLAS_INCLUDE *:= *\).*/BLAS_INCLUDE := \/opt\/openblas\/include/" Makefile.config
sed -i "s/\(# BLAS_LIB *:= *\).*/BLAS_LIB := \/opt\/openblas\/lib/" Makefile.config
sed -i "s/\(# CPU_ONLY.*\).*/CPU_ONLY := 1/" Makefile.config
sed -i "s/\(INCLUDE_DIRS.*\).*/\1 \/usr\/include\/hdf5\/serial\//" Makefile.config # for ubuntu 15.04
sed -i "s/\(LIBRARY_DIRS.*\).*/\1 \/usr\/lib\/x86_64-linux-gnu\/hdf5\/serial\/ := 1/" Makefile.config # for ubuntu 15.04
make all -j8
make test
make runtest
